Search Service
—————————
1.	Generate the build by running “sbt dist “ on the directory where git repo is cloned.
2.	After running the above command, there will be .zip file generated in the following path.
target/universal/search-service-XX-SNAPSHOT.zip
3.	Copy the build to the server machine - Sample scp command is below
scp -i ~/Documents/hotel_sit.pem  target/universal/supplier-static-content-service-1.1-SNAPSHOT.zip cloud-user@10.147.106.87:~/
4.	SSH to server - ssh -I ~/Documents/hotel_sit.pem cloud-user@10.147.106.87
5.	Installation directory of the service is  - /opt/apps/hotels-search-service/current/
6.	Stop the current running service - sudo service search-service stop
7.	change directory to - /opt/apps/hotels-search-service/releases/
8.	Remove the current build sudo rm -rf search-service
9.	Unzip the new build - sudo unzip ~/search-service-XX-SNAPSHOT.zip
10.	create the softlink to the unzipped directory - sudo ln -s /opt/apps/hotels-search-service/current/ /opt/apps/hotels-search-service/releases/search-service-XX-SNAPSHOT.zip
11.	Run the following command to start the application -  sudo service search-service start
12.	Check if the app is running - sudo service search-service status
You should see the following output
● search-service.service - Hotels Search Micro Service
   Loaded: loaded (/etc/systemd/system/search-service.service; disabled; vendor preset: disabled)
   Active: active (running) since Fri 2019-02-22 12:54:25 IST; 6min ago
  Process: 4813 ExecStop=/bin/kill -s TERM $MAINPID (code=exited, status=0/SUCCESS)
